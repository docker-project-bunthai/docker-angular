const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')

const PORT = 4300;
const app = express();
app.use(bodyParser.json());
app.use(cors())

app.get('/', function(req, res) {
	res.send('Hello from server')
})

app.get('/user', function(req, res){
  var fs = require('fs');
  var obj;
  fs.readFile('data', 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);
    console.log(obj)
  });
})

app.post('/user', function(req, res) {
  console.log(req.body)
  res.send({"message": "Data received"});
})

app.listen(PORT, function(){
  console.log("Server running on localhost:" + PORT);
});