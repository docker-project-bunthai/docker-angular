import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { fbind } from 'q';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  constructor(private fb: FormBuilder, private employeeService: EmployeeService) { }
  employeeForm = this.fb.group({
    email: this.fb.control(''),
    password: this.fb.control('')
  });

  onSubmit() {
    console.log(this.employeeForm.value);
    this.employeeService.postUser(this.employeeForm.value);
  }

  ngOnInit() {
  }

}
